process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";

import request, { AxiosError } from "axios";
import * as fs from "fs";

import { categories, products } from "./data";

const second = 1000;
const minute = 60 * second;
const hour = 60 * minute;

// const domain = "https://35.235.79.23";
const domain = "https://v3.iherb.com";

const cookie = "ih-exp-v3-force=1;";
const userAgent =
  "Mozilla/5.0 (iPhone; CPU iPhone OS 11_0 like Mac OS X) AppleWebKit/604.1.38 (KHTML, like Gecko) Version/11.0 Mobile/15A372 Safari/604.1";

const duration = minute * 5;
const interval = second / 15;

let total = 0;
let success = 0;
let error = 0;

const getInterval = (time, list, getUrl, type) => {
  return setInterval(() => {
    ++total;

    console.log(`success: ${success}; error: ${error}; total: ${total}`);

    const index = Math.floor(Math.random() * list.length);
    const id = list[index];
    const url = getUrl(id);

    request
      .get(`${url}`, {
        headers: {
          cookie,
          ["user-agent"]: userAgent
        }
      })
      .then(({ status, data }) => {
        if (data.indexOf("data-reactroot") !== -1) {
          ++success;
        } else {
          ++error;
        }
      })
      .catch((e: AxiosError) => {
        ++error;
      });
  }, time);
};

(async () => {
  console.log("starting");

  const intervalCategories = getInterval(
    interval,
    categories,
    id => `${domain}/c/${id}`,
    "categories"
  );

  // const intervalProducts = getInterval(
  //   interval,
  //   products,
  //   id => `${domain}/pr/thomas/${id}`,
  //   "products"
  // );

  setTimeout(() => {
    clearInterval(intervalCategories);
    // clearInterval(intervalProducts);

    console.log("ending");
  }, duration);
})();
